//
//  SceneDelegate.swift
//  MySpots
//
//  Created by Don Clore on 1/13/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

  var window: UIWindow?
  
  func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
    guard let windowScene = scene as? UIWindowScene else {
      return
    }
    
    let frame = windowScene.coordinateSpace.bounds
    window =  UIWindow(frame: frame)
    
    // '"Friends don't let friends force-unwrap optionals'.   From Paul Hudson's 'Exasperated Swift Developer Calendar'.
    // And I agree with it wholeheartedly, except...for exceptions like this, where both, you can see 'window' being created
    // above, and moreoever...if it's nil we want to stop in our tracks right here, and figure out what we did wrong.
    // So I use the 'bang' here.
    window!.windowScene = windowScene
    let vc = RootViewController()
    window!.rootViewController = vc
    window!.makeKeyAndVisible()
  }

  func sceneDidDisconnect(_ scene: UIScene) {
  }

  func sceneDidBecomeActive(_ scene: UIScene) {
    SpottyServices.shared.connect()
  }

  func sceneWillResignActive(_ scene: UIScene) {
    // Really, to support multiple windows on iPad, probably have to do some sort of countdown latch thingy.
    // Not gonna do that here, 'cause....it's an interview homework, but I hope you can see that I understand the issue,
    // and would implement it; honestly, I don't yet know enough about Spotify's API semantics to know for sure exactly
    // what's required.  But I have a pretty good idea that you probably don't really want to disconnect every time a window
    // gets closed.  
    SpottyServices.shared.disconnect()
  }

  func sceneWillEnterForeground(_ scene: UIScene) {
  }

  func sceneDidEnterBackground(_ scene: UIScene) {
  }
  
  func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
    guard let first = URLContexts.first else {
      return
    }
    
    SpottyServices.shared.handle(urlContext: first)
  }
}


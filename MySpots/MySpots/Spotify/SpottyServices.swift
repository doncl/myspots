//
//  SpottyConfig.swift
//  MySpots
//
//  Created by Don Clore on 1/16/21.
//

import Foundation

// Look, this is very tight cohesion to Spotify, which is not the way I'd expect production code to work at all.
// I'd expect to define a music services protocol here that encompasses business goals, and implements it in terms of
// Spotify, Apple Music, Tidal, whatever (I have no idea who the players are).

//  I've made an attempt to make SpottyServices a singleton, without stomping all over the Law of Demeter too much, but
// honestly, I don't know enough about this yet for the right 'architecture' or OO design, or whatever smart-guy words we
// want to use.  Spotify's sample quick start code is probably not a great example of how one would want to do things, unless
// you're totally OK, with complete vendor lock-in, which seems...to me like a bad idea. 
// Bottom line, crawl....walk....run....  this is crawl, and the cohesion with Spotify is great than is
// optimal.   If we want to talk about an abstract interface/protocol for all this, there's things I'd need to understand:
//
// 0.  What services do we use of a music provider to provide our use cases?
// 1.  What music providers do we want to use?
// 2.  Are the things we want to do, supported by all these music providers?   If not, can we live with graceful
//     degradation, when a bit of functionality that we want or like is supported by one provider, and not others?
// 3.  (I'm making myself nauseous with pompous software architect talk).

enum TrackState {
  case playing
  case paused
}

protocol SpottyServicesDelegate: class {
  func updateWith(trackName: String?, state: TrackState)
  func imageLanded(image: UIImage?, error: Error?)
  func stateChanged(connected: Bool)
  func needsImage() -> Bool
}

class SpottyServices: NSObject {
  static let shared: SpottyServices = SpottyServices()
  
  private var deliberatelyDisconnected: Bool = false
  
  var accessToken: String? {
    return appRemote.connectionParameters.accessToken
  }
  
  var delegates = NSPointerArray.weakObjects()
  
  private var lastPlayerState: SPTAppRemotePlayerState?
  
  private lazy var nativeSpotifyConfig: SPTConfiguration = {
    let spotifyClientID = "d62db7a44a3645789b7453e3707e81df"
    let spotifyRedirectURL = URL(string: "myspots-homework-project://myspots-callback")!

    let config = SPTConfiguration(clientID: spotifyClientID, redirectURL: spotifyRedirectURL)
    config.playURI = ""
    
    // to run this homework project you're going to have to set up a server to provide swap and refresh endpoints.
    // I probably shouldn't embed my client secrets in gitlab, I guess....so I'm not going to check in the little
    // ruby script that came in the Spotify sample.
    config.tokenSwapURL = URL(string: "http://10.5.1.188:1234/swap")
    config.tokenRefreshURL = URL(string: "http://10.5.1.188:1234/refresh")
    return config
  }()
  
  private lazy var sessionManager: SPTSessionManager = {
    let manager = SPTSessionManager(configuration: nativeSpotifyConfig, delegate: self)
    return manager
  }()

  private lazy var appRemote: SPTAppRemote = {
    let appRemote = SPTAppRemote(configuration: nativeSpotifyConfig, logLevel: .debug)
    appRemote.delegate = self
    return appRemote
  }()
  
  var hasToken: Bool {
    return appRemote.connectionParameters.accessToken != nil
  }
  
  var isConnected: Bool {
    return appRemote.isConnected
  }
  
  func initiateSession() {
    let scope: SPTScope = [.appRemoteControl, .playlistReadPrivate, .playlistModifyPublic, .playlistModifyPrivate]
    
    sessionManager.initiateSession(with: scope, options: AuthorizationOptions.clientOnly)
  }
  
  func connect() {
    if let _ = appRemote.connectionParameters.accessToken {
      deliberatelyDisconnected = false
      appRemote.connect()
    }
  }
  
  func disconnect() {
    appRemote.playerAPI?.pause(nil)
    if appRemote.isConnected {
      deliberatelyDisconnected = true
      appRemote.disconnect()
    }
  }
  
  func pauseOrPlay() {
    if let lastPlayerState = lastPlayerState, lastPlayerState.isPaused {
      appRemote.playerAPI?.resume(nil)
    } else {
      appRemote.playerAPI?.pause(nil)
    }
  }
      
  private func fetchPlayerState() {
    appRemote.playerAPI?.getPlayerState({ [weak self] (playerState, error) in
      guard let self = self else { return }
      if let error = error {
        print("Error getting player state: \(error.localizedDescription)")
      } else if let playerState = playerState as? SPTAppRemotePlayerState {
        self.update(playerState: playerState)
      }
    })
  }
  
  private func performOnDelegates(action: (SpottyServicesDelegate) -> ()) {
    for obj in delegates.allObjects {
      if let del = obj as? SpottyServicesDelegate {
        action(del)
      }
    }
  }
  
  private func update(playerState: SPTAppRemotePlayerState) {
    lastPlayerState = playerState
    
    var needsImage: Bool = false
    performOnDelegates(action: { del in
      if del.needsImage() {
        needsImage = true
      }
    })
    if lastPlayerState?.track.uri != playerState.track.uri || needsImage {
      fetchArtwork(forTrack: playerState.track)
    }
    let state = playerState.isPaused ? TrackState.paused : TrackState.playing
    
    performOnDelegates(action: { del in
      del.updateWith(trackName: playerState.track.name, state:state)
    })
  }
  
  private func fetchArtwork(forTrack track: SPTAppRemoteTrack) {
    appRemote.imageAPI?.fetchImage(forItem: track, with: CGSize.zero, callback: { [weak self] image, error in
      guard let self = self else { return }
      
      // Evidently...the Spotify API has a single callback signature type, which just returns a nillable id-any in the
      // first argument, and an optional Error in the second.  Not...my choice to do things that way, but that's what we have
      // here.
      if let error = error {
        self.performOnDelegates(action: { del in
          del.imageLanded(image: nil, error: error)
        })
      } else if let uiimage = image as? UIImage {
        self.performOnDelegates(action: { del in
          del.imageLanded(image: uiimage, error: nil)
        })
      }
    })
  }
  
  func handle(urlContext: UIOpenURLContext) {
    let app = UIApplication.shared
    
    let ctxtOptions = urlContext.options
    let options: [UIApplication.OpenURLOptionsKey: Any] = [
      UIApplication.OpenURLOptionsKey.sourceApplication: ctxtOptions.sourceApplication as Any,
      UIApplication.OpenURLOptionsKey.annotation: ctxtOptions.annotation as Any,
      UIApplication.OpenURLOptionsKey.openInPlace: ctxtOptions.openInPlace as Any,
    ]

    sessionManager.application(app, open: urlContext.url, options: options)
    guard let parameters: [String: String] = appRemote.authorizationParameters(from: urlContext.url) else {
      return
    }

    if let accessToken = parameters[SPTAppRemoteAccessTokenKey] {
      appRemote.connectionParameters.accessToken = accessToken
    } else if let error = parameters[SPTAppRemoteErrorDescriptionKey] {
        print("FIRE FEAR FOES - \(error)")
    }
  }
  
  private override init() {
    
  }
}

// MARK: SPTSessionManagerDelegate
extension SpottyServices: SPTSessionManagerDelegate {
  func sessionManager(manager: SPTSessionManager, didInitiate session: SPTSession) {
    appRemote.connectionParameters.accessToken = session.accessToken
    appRemote.connect()
  }
  
  func sessionManager(manager: SPTSessionManager, didRenew session: SPTSession) {
    print("\(#function)")
  }
  
  func sessionManager(manager: SPTSessionManager, didFailWith error: Error) {
    print("\(#function)")
  }
}

// MARK: SPTAppRemoteDelegate
extension SpottyServices: SPTAppRemoteDelegate {
  func appRemoteDidEstablishConnection(_ appRemote: SPTAppRemote) {
    appRemote.playerAPI?.delegate = self
    appRemote.playerAPI?.subscribe(toPlayerState: { (success, error) in
      if let error = error {
        print("\(#function) Error subscribing to player state: \(error.localizedDescription)")
      }
    })
    fetchPlayerState()
    performOnDelegates(action: { del in
      del.stateChanged(connected: true)
    })
  }
  
  func appRemote(_ appRemote: SPTAppRemote, didFailConnectionAttemptWithError error: Error?) {
    print("\(#function)")
    performOnDelegates(action: { del in
      del.stateChanged(connected: false)
    })
  }
  
  func appRemote(_ appRemote: SPTAppRemote, didDisconnectWithError error: Error?) {
    print("\(#function)")
    if !deliberatelyDisconnected {
      initiateSession()
    }

    performOnDelegates(action: { del in
      del.stateChanged(connected: false)
    })
  }
}

// MARK: SPTAppRemovePlayerStateDelegate
extension SpottyServices: SPTAppRemotePlayerStateDelegate {
  func playerStateDidChange(_ playerState: SPTAppRemotePlayerState) {
    update(playerState: playerState)
  }  
}

//https://marcosantadev.com/swift-arrays-holding-elements-weak-references/
extension NSPointerArray {
  func addObject(_ object: AnyObject?) {
    guard let strongObject = object else { return }

    let pointer = Unmanaged.passUnretained(strongObject).toOpaque()
    addPointer(pointer)
  }

  func insertObject(_ object: AnyObject?, at index: Int) {
    guard index < count, let strongObject = object else { return }

    let pointer = Unmanaged.passUnretained(strongObject).toOpaque()
    insertPointer(pointer, at: index)
  }

  func replaceObject(at index: Int, withObject object: AnyObject?) {
    guard index < count, let strongObject = object else { return }

    let pointer = Unmanaged.passUnretained(strongObject).toOpaque()
    replacePointer(at: index, withPointer: pointer)
  }

  func object(at index: Int) -> AnyObject? {
    guard index < count, let pointer = self.pointer(at: index) else { return nil }
    return Unmanaged<AnyObject>.fromOpaque(pointer).takeUnretainedValue()
  }

  func removeObject(at index: Int) {
    guard index < count else { return }

    removePointer(at: index)
  }
}

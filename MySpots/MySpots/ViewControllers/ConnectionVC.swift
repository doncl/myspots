//
//  ConnectionVC.swift
//  MySpots
//
//  Created by Don Clore on 1/17/21.
//

import UIKit

class ConnectionVC: UIViewController {
  struct Constants {
    static let topPad: CGFloat = 8.0
    static let btnWidth: CGFloat = 280.0
    static let btnHeight: CGFloat = 48.0
    static let btnCornerRadius: CGFloat = 24.0
    static let bottomPad: CGFloat = 8.0
    static let connectLabelSize: CGSize = CGSize(width: 200, height: 30)
    static let disconnectedText: String = "Disconnected"
    static let connectedText: String = "Connected"
    static let lblCornerRadius: CGFloat = 18.0
  }
  
  private lazy var connectBtn: UIButton = {
    let button = makeAConnectButton(withText: "Spot me!  (connect to Spotify)")
    return button
  }()
  
  private lazy var connectLabel: UILabel = {
    let l = UILabel()
    l.translatesAutoresizingMaskIntoConstraints = false
    l.textColor = UIColor(named: "connectbtntextcolor")!
    l.backgroundColor = UIColor(named: "connectlabelbackgroundcolor")!
    l.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.medium)
    l.text = Constants.disconnectedText
    l.textAlignment = NSTextAlignment.center
    l.clipsToBounds = true
    return l
  }()
  
  private lazy var disConnectBtn: UIButton = {
    let button = makeAConnectButton(withText:  "Disconnect, Yo")
    return button
  }()
  
  let player: PlayerVC = PlayerVC()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor(named: "logindlgbackground")!
    
    view.addSubview(connectLabel)
    view.addSubview(connectBtn)
    view.addSubview(disConnectBtn)

    connectLabel.layer.cornerRadius = Constants.lblCornerRadius
    
    addChild(player)
    view.addSubview(player.view)
    player.didMove(toParent: self)
    player.view.translatesAutoresizingMaskIntoConstraints = false
    player.delegate = self
            
    NSLayoutConstraint.activate([
      connectLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Constants.topPad),
      connectLabel.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
      connectLabel.widthAnchor.constraint(equalToConstant: Constants.connectLabelSize.width),
      connectLabel.heightAnchor.constraint(equalToConstant: Constants.connectLabelSize.height),
      
      player.view.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      player.view.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      player.view.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
      player.view.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
      
      connectBtn.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
      connectBtn.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
      connectBtn.widthAnchor.constraint(equalToConstant: Constants.btnWidth),
      connectBtn.heightAnchor.constraint(equalToConstant: Constants.btnHeight),
      
      disConnectBtn.centerXAnchor.constraint(equalTo: connectBtn.centerXAnchor),
      disConnectBtn.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -Constants.bottomPad),
      disConnectBtn.widthAnchor.constraint(equalTo: connectBtn.widthAnchor),
      disConnectBtn.heightAnchor.constraint(equalTo: connectBtn.heightAnchor),
    ])
    
    
    connectBtn.addTarget(self, action: #selector(ConnectionVC.connectBtnTouched(_:)), for: UIControl.Event.touchUpInside)
    disConnectBtn.addTarget(self, action: #selector(ConnectionVC.disConnectBtnTouched(_:)),
                            for: UIControl.Event.touchUpInside)
        
    SpottyServices.shared.delegates.addObject(self)
    updateViewBasedOnConnected()
  }

  // Slavish adaptation from Spotify API sample.
  func updateViewBasedOnConnected() {
    if (SpottyServices.shared.isConnected) {
      connectBtn.isHidden = true
      disConnectBtn.isHidden = false
      connectLabel.text = Constants.connectedText
      player.view.isHidden = false
    } else {
      disConnectBtn.isHidden = true
      connectBtn.isHidden = false
      connectLabel.text = Constants.disconnectedText
      player.view.isHidden = true
    }
  }
}

extension ConnectionVC {
  private func makeAConnectButton(withText text: String) -> UIButton {
    let button = UIButton(type: .custom)
    button.translatesAutoresizingMaskIntoConstraints = false
    let backgroundColor: UIColor = UIColor(named: "connectbtnbackgroundcolor")!
    
    let attrs: [NSAttributedString.Key: Any] = [
      NSAttributedString.Key.foregroundColor: UIColor(named: "connectbtntextcolor")!,
      NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.semibold),
      NSAttributedString.Key.backgroundColor: backgroundColor,
    ]
    
    button.backgroundColor = backgroundColor
    let title: NSAttributedString = NSAttributedString(string: text, attributes: attrs)
    
    button.setAttributedTitle(title, for: UIControl.State.normal)
    button.layer.cornerRadius = Constants.btnCornerRadius
    
    return button

  }
  @objc func connectBtnTouched(_ sender: UIButton) {
    SpottyServices.shared.initiateSession()
  }
  
  @objc func disConnectBtnTouched(_ sender: UIButton) {
    SpottyServices.shared.disconnect()
  }
  
  @objc func didTapPauseOrPlay(_ button: UIButton) {
    SpottyServices.shared.pauseOrPlay()
  }
}

extension ConnectionVC: SpottyServicesDelegate {
  func needsImage() -> Bool {
    return player.needsImage
  }
  
  func updateWith(trackName: String?, state: TrackState) {
    print("\(#function)")
    
    player.updateWith(trackName: trackName, state: state)
  }
  
  func imageLanded(image: UIImage?, error: Error?) {
    print("\(#function)")
    player.imageLanded(image: image, error: error)
  }
  
  func stateChanged(connected: Bool) {
    print("\(#function)")
    updateViewBasedOnConnected()
  }
}

extension ConnectionVC: PlayerVCDelegate {
  func tappedPausePlay(_ button: UIButton) {
    didTapPauseOrPlay(button)
  }
}



//
//  SearchVC.swift
//  MySpots
//
//  Created by Don Clore on 1/17/21.
//

import UIKit

class SearchVC: UIViewController {
  let searchBar: UISearchBar = UISearchBar()
  let resultsController: SearchResultsVC = SearchResultsVC()
  let spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
  var alreadyLaidOut: Bool = false
  
  let gutter: CGFloat = 16.0

  init() {
    super.init(nibName: nil, bundle: nil)
    searchBar.delegate = self
    searchBar.placeholder = "Search for tunes"
    searchBar.showsCancelButton = true
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.addSubview(searchBar)
    
    searchBar.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      searchBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
      searchBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
      searchBar.heightAnchor.constraint(equalToConstant: 56)
    ])
    addChild(resultsController)
    view.addSubview(resultsController.view)
    resultsController.didMove(toParent: self)
    
    let rv: UIView = resultsController.view
    rv.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      rv.topAnchor.constraint(equalTo: searchBar.bottomAnchor),
      rv.leadingAnchor.constraint(equalTo: searchBar.leadingAnchor),
      rv.trailingAnchor.constraint(equalTo: searchBar.trailingAnchor),
      rv.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if !alreadyLaidOut {
      searchBar.autocapitalizationType = .none
      searchBar.returnKeyType = .done
      alreadyLaidOut = true
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    definesPresentationContext = true
  }
}

extension SearchVC: UISearchBarDelegate {
  
}



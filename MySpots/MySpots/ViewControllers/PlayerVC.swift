//
//  PlayerVCViewController.swift
//  MySpots
//
//  Created by Don Clore on 1/17/21.
//

import UIKit

protocol PlayerVCDelegate: class {
  func tappedPausePlay(_ button: UIButton)
}

class PlayerVC: UIViewController {
  struct Constants {
    static let imageGutters: CGFloat = 16.0
    static let playBtnDim: CGFloat = 60.0
    static let topPad: CGFloat = 8.0
    static let bottomPad: CGFloat = 8.0
  }
  
  var imageWidth: NSLayoutConstraint = NSLayoutConstraint()
  var imageHeight: NSLayoutConstraint = NSLayoutConstraint()
  
  var needsImage: Bool = true
  
  weak var delegate: PlayerVCDelegate?
  
  private lazy var trackLabel: UILabel = {
    let trackLabel = UILabel()
    trackLabel.translatesAutoresizingMaskIntoConstraints = false
    trackLabel.textAlignment = .center
    trackLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
    trackLabel.textColor = UIColor(named: "tracklabeltextcolor")!
    return trackLabel
  }()

  private lazy var pauseAndPlayButton: UIButton = {
    let button = UIButton()
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()

  private lazy var imageView: UIImageView = {
    let imageView = UIImageView()
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.contentMode = .scaleAspectFit
    imageView.image = UIImage(named: "noimage")!
    return imageView
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.addSubview(trackLabel)
    view.addSubview(imageView)
    view.addSubview(pauseAndPlayButton)
        
    imageWidth = imageView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor, constant: -Constants.imageGutters)
    imageHeight = imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1)
    
    NSLayoutConstraint.activate([
      trackLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Constants.topPad),
      trackLabel.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
      
      imageView.topAnchor.constraint(equalTo: trackLabel.bottomAnchor, constant: Constants.bottomPad),
      imageWidth,
      imageHeight,
      imageView.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
      
      pauseAndPlayButton.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: Constants.topPad),
      pauseAndPlayButton.centerXAnchor.constraint(equalTo: imageView.centerXAnchor),
      pauseAndPlayButton.widthAnchor.constraint(equalToConstant: Constants.playBtnDim),
      pauseAndPlayButton.heightAnchor.constraint(equalToConstant: Constants.playBtnDim),
      
      pauseAndPlayButton.bottomAnchor.constraint(equalTo: view.bottomAnchor),
    ])
    
    pauseAndPlayButton.addTarget(self, action: #selector(PlayerVC.didTapPauseOrPlay(_:)),
                                 for: UIControl.Event.touchUpInside)
  }
}

extension PlayerVC {
  @objc func didTapPauseOrPlay(_ sender: UIButton) {
    guard let delegate = delegate else {
      return
    }
    
    delegate.tappedPausePlay(sender)
  }
  
  func imageLanded(image: UIImage?, error: Error?) {
    print("\(#function)")
    imageHeight.isActive = false
    if let image = image {
      imageView.image = image
      if image.size.width > 0 {
        let ratio = image.size.height / image.size.width
        imageHeight = imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: ratio)
      }
      needsImage = false
    } else if let error = error {
      print(error.localizedDescription)
      imageView.image = UIImage(named: "noimage")!
      imageHeight = imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1)
      needsImage = true
    }
    imageHeight.isActive = true
  }
  
  func updateWith(trackName: String?, state: TrackState) {
    print("\(#function)")

    trackLabel.text = trackName
    
    let image: UIImage
    switch state {
    case .playing:
      image = UIImage(named: "pause")!
    case .paused:
      image = UIImage(named: "play")!
    }
    
    pauseAndPlayButton.setImage(image, for: UIControl.State.normal)
  }
}

// MARK: PlayerVCDelegate
extension PlayerVC {
  
}

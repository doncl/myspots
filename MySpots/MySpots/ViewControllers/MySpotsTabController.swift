//
//  MySpotsTabController.swift
//  MySpots
//
//  Created by Don Clore on 1/17/21.
//

import UIKit

class MySpotsTabController: UITabBarController {
  let connect = ConnectionVC()
  let search = SearchVC()
  let library = LibraryVC()
  
  init() {
    super.init(nibName: nil, bundle: nil)
    viewControllers = [connect, search, library]
    
    connect.tabBarItem = UITabBarItem(title: "Connect", image: UIImage(named: "connect")!, tag: 0)
    search.tabBarItem = UITabBarItem(title: "Search", image: UIImage(named: "search")!, tag: 1)
    library.tabBarItem = UITabBarItem(title: "Library", image: UIImage(named: "library")!, tag: 2)
    enableNonConnectionTabs(enable: false)
    SpottyServices.shared.delegates.addObject(self)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension MySpotsTabController: SpottyServicesDelegate {
  func updateWith(trackName: String?, state: TrackState) {
  }
  
  func imageLanded(image: UIImage?, error: Error?) {
  }
  
  func stateChanged(connected: Bool) {
    enableNonConnectionTabs(enable: connected)
    if !connected {
      selectedIndex = 0
    }
  }
  
  func needsImage() -> Bool {
    return false
  }
  
  private func enableNonConnectionTabs(enable: Bool) {
    [search, library].forEach {
      $0.tabBarItem.isEnabled = enable
    }
  }
}

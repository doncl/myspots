//
//  ViewController.swift
//  MySpots
//
//  Created by Don Clore on 1/13/21.
//

import UIKit

class RootViewController: UIViewController {
  let tab: MySpotsTabController = MySpotsTabController()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addChild(tab)
    view.addSubview(tab.view)
    tab.didMove(toParent: self)
    
    tab.view.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      tab.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      tab.view.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      tab.view.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
      tab.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
  }
  
}
// MARK: Crude error dialog
extension UIViewController {
  func alert(title: String, message: String, buttonTitle: String) {
    let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let action = UIAlertAction(title: buttonTitle, style: .default, handler: nil)
    ac.addAction(action)
    present(ac, animated: true)
  }
}


